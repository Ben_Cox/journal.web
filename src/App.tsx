import './App.css';
import { AuthProvider } from './contexts/authContext';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomeScreen from './screens/home/home';
import SignInScreen from './screens/signIn/signIn';
import JournalsScreen from './screens/journals/journals';
import NewJournalScreen from './screens/journals/new/newJournal';
import ExistingJournalScreen from './screens/journals/existing/existingJournal';

function App() {
	return (
		<main>
			<BrowserRouter>
				<AuthProvider>
					<Routes>
						<Route path='/' element={<HomeScreen />} />
						<Route path='/signin' element={<SignInScreen />} />
						<Route path='/journals' element={<JournalsScreen />} />
						<Route path='/journals/new' element={<NewJournalScreen />} />
						<Route path='/journals/:id' element={<ExistingJournalScreen />} />
					</Routes>
				</AuthProvider>
			</BrowserRouter>
		</main>
	);
}

export default App;
