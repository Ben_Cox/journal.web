export interface IAuthContext {
	user?: IUser,
	signIn: (userName: string, password: string) => void,
	signInLoading: boolean,
	signInErrored: boolean,
	signOut: () => void,
	signOutLoading: boolean,
	signOutErrored: boolean,

}

export interface IUser {
	userName: string,
}