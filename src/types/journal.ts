export interface JournalSummary {
	id: number,
	title: string,
	authorUserName: string,
	date: Date,
}

export interface IJournal  {
	id?: number,
	title: string,
	content: string,
	authorUserName?: string,
	date: Date,
}