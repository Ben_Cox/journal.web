import { faArrowLeft, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useCallback, useEffect, useState } from "react";
import DateTimePicker from "react-datetime-picker";
import { Link, useNavigate } from "react-router-dom";
import { IJournal } from "../../types/journal";
import useStyles from "./styles"

interface IProps {
	readOnly?: true,
	journal?: IJournal
}

const JournalForm: React.FC<IProps> = ({ readOnly, journal }) => {
	const classes = useStyles();

	const [journalFormData, setJournalFormData] = useState<IJournal>({ title: '', date: new Date(), content: '', ...journal });
	const [journalFormDataErrorMessages, setJournalFormDataErrorMessages] = useState<{ [k: string]: string }>();

	const [submitLoading, setSubmitLoading] = useState(false);
	const [submitErrored, setSubmitErrored] = useState(false);

	const navigate = useNavigate();

	const validateTitle = () => {
		let message: string = '';

		if (!journalFormData?.title)
			message = "Please provide a title";

		if (journalFormData.title.length > 100)
			message = "You can only have 100 characters";

		setJournalFormDataErrorMessages(prev => ({ ...prev, title: message }));
		return !message;
	}

	const validateContent = () => {
		let message: string = '';

		if (!journalFormData?.content)
			message = "Please provide some details";

		setJournalFormDataErrorMessages(prev => ({ ...prev, content: message }));
		return !message;
	}

	const validateDate = useCallback(() => {
		let message: string = '';

		if (!journalFormData?.date)
			message = "Please provide a date";

		if (journalFormData?.date > new Date())
			message = "Please provide a date in the past"

		setJournalFormDataErrorMessages(prev => ({ ...prev, date: message }));
		return !message;
	}, [journalFormData?.date]);

	const handleSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
		e.preventDefault(); // Don't refresh the page
		if (!readOnly) {
			// Validation
			// Done as vars so all messages show
			const titleOk = validateTitle();
			const dateOk = validateDate();
			const contentOk = validateContent();
			if (titleOk && dateOk && contentOk) {
				setSubmitLoading(true);
				axios.post<IJournal>(`${process.env.REACT_APP_BASE_API_URL}api/journals/`, journalFormData, { withCredentials: true })
					.then(() => {												
						navigate('/journals/');
					})
					.catch(error => {
						console.error(error)
						setSubmitErrored(true);
						setSubmitLoading(false); // no in finally as we navigate away on success, so no need to stop the spinner in that case
					});
			}
		}
	};

	useEffect(() => {
		if (!readOnly)
			validateDate();
	}, [journalFormData.date, readOnly, validateDate]);

	return (
		<form id="journal-form" onSubmit={handleSubmit} className={classes.form}>
			<Link className={classes.backArrow} to='/journals/'><FontAwesomeIcon icon={faArrowLeft} /></Link>
			<label htmlFor="date-input">Date: </label>
			<DateTimePicker
				name="date-input"
				disabled={readOnly}
				maxDate={new Date()}
				className={classes.datePicker}
				value={journalFormData?.date}
				onChange={(v) => setJournalFormData(prev => ({ ...prev, date: v }))}
			/>
			{journalFormDataErrorMessages?.date && <div id="date-error-message" className={classes.errorMessage}>{journalFormDataErrorMessages.date}</div>}

			<label htmlFor="title-input">Title: </label>
			<input
				id="username-input"
				readOnly={readOnly}
				type="text"
				placeholder="A 100 character TL;DR brief"
				value={journalFormData?.title}
				onChange={(e) => setJournalFormData(prev => ({ ...prev, title: e.target.value }))}
				onBlur={() => validateTitle()}
			/>
			{journalFormDataErrorMessages?.title && <div id="title-error-message" className={classes.errorMessage}>{journalFormDataErrorMessages.title}</div>}

			<label htmlFor="content-input">Content: </label>
			<textarea
				id="content-input"
				readOnly={readOnly}
				rows={5}
				placeholder="Say as much as you want"
				value={journalFormData?.content}
				onChange={(e) => setJournalFormData(prev => ({ ...prev, content: e.target.value }))}
				onBlur={() => validateContent()}
			/>
			{journalFormDataErrorMessages?.content && <div id="content-error-message" className={classes.errorMessage}>{journalFormDataErrorMessages.content}</div>}

			{!readOnly &&
				<>
					<button id="journal-submit" type="submit" disabled={readOnly}>{!submitLoading ? "Create" : <FontAwesomeIcon icon={faSpinner} fixedWidth spin />}</button>
					{submitErrored && <div id="journal-submit-error-message" className={classes.errorMessage}>Sorry, something went wrong</div>}
				</>
			}
		</form>
	);
};

export default JournalForm;