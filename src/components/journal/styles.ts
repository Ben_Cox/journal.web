import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
	form: {
		display: "block",
		margin: "0 auto",
		maxWidth: 350,
		padding: 10,		
		textAlign: "left",
		"& > label": {
			// width: "100%",
			// display: "block",
			maxWidth: 200,
			fontSize: 16,
			fontWeight: "bold",
			margin: 0,
			marginBottom: 5,
			display: "block",
		},
		"& > input": {
			width: "100%",
			// maxWidth: 200,
			margin: "10px 0",
			display: "block",
			// borderRadius: 15,
			border: "none",
			borderBottom: "1px solid grey",
		},
		"& > textarea": {
			display: "block",
			resize: "vertical",
			width: "100%",
			margin: "10px 0",
		},
		"& > button": {
			display: "block",
			maxWidth: "50%",
			margin: "0 auto",
			padding: 15,
			borderRadius: 30,
			textDecoration: "none",
			minWidth: 75,
			textAlign: "center",
			color: "white",
			backgroundColor: "#ED0B6F",
			border: "1px solid #ED0B6F",
			transition: "background-color .4s ease",
			WebkitTransition: "background-color .4s ease",
			MozTransition: "background-color .4s ease",
			"&:hover": {
				color: "#ED0B6F",
				backgroundColor: "transparent",
			},
		}
	},
	datePicker: {
		marginBottom: 10,
		"& > div": {
			border: "none",
			borderBottom: "1px solid grey",
		}
	},
	errorMessage: {
		color: "red"
	},	
	backArrow: {
		position: "absolute",
		top: 15,
		left: 15,
		fontSize: 15,
		color: "black",
		textDecoration: "none"
	}
});

export default useStyles;