import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
	navbar: {
		padding: 10,
		marginBottom: 15,
		display: "flex",
		justifyContent: "center",
		height: 70,
	},
	desktopContentContainer: {
		display: "flex",
		backgroundColor: "white",
		borderRadius: 30,
		padding: 10,
		height: "auto",
		alignItems: "center",
	},
	callToAction: {
		backgroundColor: "transparent",
		padding: 15,
		marginLeft: 15,
		borderRadius: 30,
		color: "black",
		textDecoration: "none",
		display: "block",
		minWidth: 75,
		textAlign: "center",
	},
	button: {
		color: "white",
		backgroundColor: "#ED0B6F",
		border: "1px solid #ED0B6F",
		transition: "background-color .4s ease",
		WebkitTransition: "background-color .4s ease",
		MozTransition: "background-color .4s ease",
		"&:hover": {
			color: "#ED0B6F",
			backgroundColor: "transparent",
		},
	},
	link: {
		transition: "background-color .4s ease",
		WebkitTransition: "background-color .4s ease",
		MozTransition: "background-color .4s ease",
		"&:hover": {
			color: "white",
			backgroundColor: "#ED0B6F",
		},
	},
	"@media only screen and (max-width: 800px)": {
		navbar: {
			justifyContent: "flex-start"
		},
		desktopContentContainer: {
			display: "none",
		}
	}
});

export default useStyles;