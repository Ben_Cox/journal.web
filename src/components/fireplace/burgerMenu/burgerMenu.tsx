import { faBars, faExternalLink, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useCallback, useContext, useEffect } from "react";
import { useRef, useState } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../../contexts/authContext";
import useStyles from "./style";


const BurgerMenu: React.FC = () => {
	const classes = useStyles();

	const { user, signOut } = useContext(AuthContext);

	const ref = useRef<HTMLDivElement>(null);
	const [isClosing, setIsClosing] = useState(false);
	const [isClosed, setIsClosed] = useState(true);

	const close = useCallback(() => {
		setIsClosing(true);
		setTimeout(() => setIsClosed(true), 600);
	}, []);

	const signOutLocal = (): void => {
		close();
		signOut();
	}
	
	// On click outside, close the menu
	useEffect(() => {
		function handleClickOutside(event: MouseEvent) {
			if (ref.current && !ref.current.contains(event.target as Node)) {
				close();
			}
		}

		document.addEventListener("mousedown", handleClickOutside);
		return () => document.removeEventListener("mousedown", handleClickOutside);
	}, [ref, close]);

	if (isClosed)
		return (
			<FontAwesomeIcon icon={faBars} size="2x" className={classes.barsIcon} onClick={() => { setIsClosed(false); setIsClosing(false); }} />
		);

	return (
		<>
			<div ref={ref} className={`${classes.burgerMenu} ${isClosing ? classes.closeAnimation : ""}`}>
				<div className={classes.burgerMenuInner}>
					<div className={classes.burgerMenuInner2}>
						<div className={`${classes.itemsContainer} ${isClosing ? classes.itemsCloseAnimation : ""}`}>
							<Link id="navbar-home-link" to="/" className={classes.link}>Home</Link>
							<Link id="navbar-journals-link" to="/journals/" className={classes.link}>Journals</Link>
							<a id="navbar-journals-link" href="https://www.thebencox.com/" target={"_blank"} rel="noreferrer" className={classes.link}>Ben Cox™ <FontAwesomeIcon icon={faExternalLink} fixedWidth /></a>
							{
								user ?
									<div id="sign-out-button" onClick={signOutLocal} className={classes.link}>Sign Out</div>
									:
									<Link id="sign-in-link" to="/signin/" className={classes.link}>Sign In</Link>
							}
						</div>
					</div>
				</div>
			</div>
			<FontAwesomeIcon icon={faTimesCircle} className={`${classes.closeBtn} ${isClosing ? classes.closeButtonCloseAnimation : ""}`} onClick={close} />
		</>
	);
}

export default BurgerMenu;