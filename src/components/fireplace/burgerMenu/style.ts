import { createUseStyles } from "react-jss";

const menuMaxWidth = "100%";
const menuMaxHeight = "65vh";
const innerMaxWidth = "90%";
const innerMaxHeight = "90%";
const closeButtonMaxBottom = 30;
const closeButtonMaxFontSize = 28;
const closeButtonMaxFontSizeDesktop = 40;

const useStyles = createUseStyles(() => ({
	"@keyframes openAnimation": {
		"0%": { width: 0, height: 0 },
		"100%": { width: menuMaxWidth, height: menuMaxHeight }
	},
	"@keyframes closeAnimation": {
		"0%": { width: menuMaxWidth, height: menuMaxHeight },
		"100%": { width: 0, height: 0 }
	},
	"@keyframes openInnerAnimation": {
		"0%": { width: 0, height: 0 },
		"100%": { width: innerMaxWidth, height: innerMaxHeight }
	},
	"@keyframes closeInnerAnimation": {
		"0%": { width: innerMaxWidth, height: innerMaxHeight },
		"100%": { width: 0, height: 0 },
	},
	"@keyframes openItemsAnimation": {
		"0%": { opacity: 0, },
		"100%": { opacity: 1, },
	},
	"@keyframes closeItemsAnimation": {
		"0%": { opacity: 1, },
		"100%": { opacity: 0 },
	},
	"@keyframes closeButtonOpenAnimation": {
		"0%": { bottom: 0, fontSize: 0 },
		"100%": { bottom: closeButtonMaxBottom, fontSize: closeButtonMaxFontSize }
	},
	"@keyframes closeButtonCloseAnimation": {
		"0%": { bottom: closeButtonMaxBottom, fontSize: closeButtonMaxFontSize },
		"100%": { bottom: 0, fontSize: 0 }
	},
	"@keyframes closeButtonOpenAnimationDesktop": {
		"0%": { bottom: 0, fontSize: 0 },
		"100%": { bottom: closeButtonMaxBottom, fontSize: closeButtonMaxFontSizeDesktop }
	},
	"@keyframes closeButtonCloseAnimationDesktop": {
		"0%": { bottom: closeButtonMaxBottom, fontSize: closeButtonMaxFontSizeDesktop },
		"100%": { bottom: 0, fontSize: 0 }
	},
	burgerMenu: {
		position: "fixed",
		zIndex: 101,
		backgroundColor: "#FCAE17",
		top: 0,
		left: 0,
		width: menuMaxWidth,
		height: menuMaxHeight,
		borderBottomRightRadius: "65%",
		animation: "$openAnimation .3s",
		maxWidth: 425,
		maxHeight: 425
	},
	closeAnimation: {
		//height: 0,
		//width: 0,
		animationName: "$closeAnimation",
		animationDuration: ".3s",
		animationDelay: ".3s",
		animationFillMode: "forwards"
	},
	burgerMenuInner: {
		height: innerMaxHeight,
		width: innerMaxWidth,
		// backgroundColor: "#800080",
		backgroundColor: "#B0BB36",
		borderBottomRightRadius: "65%",
		animation: "$openInnerAnimation .3s",
	},
	burgerMenuInner2: {
		height: innerMaxHeight,
		width: innerMaxWidth,
		// backgroundColor: "#eabfff",
		backgroundColor: "white",
		borderBottomRightRadius: "65%",
		animation: "$openInnerAnimation .3s",
		display: "flex",
		alignItems: "flex-start"
	},
	itemsContainer: {
		display: "flex",
		flexDirection: "row",
		flexWrap: "wrap",
		justifyContent: "space-around",
		alignItems: "center",
		height: "85%",
		width: "85%",
		margin: "0 auto",
		paddingRight: "10%",

		opacity: 0,
		animationName: "$openItemsAnimation",
		animationDuration: ".3s",
		animationDelay: ".3s",
		animationFillMode: "forwards" // Keep keyframe values after animation finishes
	},
	itemsCloseAnimation: {
		opacity: 1,
		animationName: "$closeItemsAnimation",
		animationDuration: ".1s",
		animationFillMode: "forwards",
	},
	closeBtn: {
		color: "#FCAE17",
		position: "fixed",
		left: "50%",
		zIndex: 101,
		animation: "$closeButtonOpenAnimation .3s",
		fontSize: closeButtonMaxFontSize,
		bottom: closeButtonMaxBottom,
		// [theme.breakpoints.up("md")]: {
		//     fontSize: closeButtonMaxFontSizeDesktop,
		//     animation: "$closeButtonOpenAnimationDesktop .3s",
		// }
	},
	closeButtonCloseAnimation: {
		//fontSize: 0,
		//bottom: 0,
		animationName: "$closeButtonCloseAnimation",
		animationDuration: ".3s",
		animationDelay: ".3s",
		animationFillMode: "forwards",
		// [theme.breakpoints.up("md")]: {
		//     animationName: "$closeButtonCloseAnimationDesktop",
		// }
	},
	link: {
		padding: 10,
		width: 100,
		textAlign: "center",
		color: "black",
		textDecoration: "none",
		"& a": {
			color: "black",
			textDecoration: "none",
		}
	},
	button: {

	},
	barsIcon: {},
	"@media only screen and (min-width: 800px)": {
		burgerMenu: {
			display: "none"
		},
		barsIcon: {
			display: "none"
		}
	},
}));

export default useStyles;