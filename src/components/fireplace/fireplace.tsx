import { faExternalLink } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { PropsWithChildren, useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../contexts/authContext";
import BurgerMenu from "./burgerMenu/burgerMenu";
import useStyles from "./styles";

const Fireplace: React.FC<PropsWithChildren> = ({children}) => {
	const classes = useStyles();

	const { user, signOut } = useContext(AuthContext);	

	return (
		<div>
			<div className={classes.navbar}>
				<BurgerMenu />
				<div className={classes.desktopContentContainer}>
					<Link id="navbar-home-link" to="/" className={`${classes.callToAction} ${classes.link}`}>Home</Link>
					<Link id="navbar-journals-link" to="/journals/" className={`${classes.callToAction} ${classes.link}`}>Journals</Link>
					<a id="navbar-journals-link" href="https://www.thebencox.com/" target={"_blank"} rel="noreferrer" className={`${classes.callToAction} ${classes.link}`}>Ben Cox™ <FontAwesomeIcon icon={faExternalLink} fixedWidth /></a>
					{ 
						user ? 
							<button id="sign-out-button" onClick={signOut} className={`${classes.callToAction} ${classes.button}`}>Sign Out</button> 
							: 
							<Link id="sign-in-link" to="/signin/" className={`${classes.callToAction} ${classes.button}`}>Sign In</Link>
					}
				</div>				
			</div>
			{children}
		</div>
	);
}

export default Fireplace;