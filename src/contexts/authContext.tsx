import axios from "axios";
import { createContext, PropsWithChildren, useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { IAuthContext, IUser } from "../types/auth";

export const AuthContext = createContext<IAuthContext>({
	user: undefined,
	signIn: () => undefined,
	signInLoading: false,
	signInErrored: false,
	signOut: () => undefined,
	signOutLoading: false,
	signOutErrored: false,
});

export const AuthProvider: React.FC<PropsWithChildren> = ({ children }) => {
	const [user, setUser] = useState<IUser>();

	const [signInLoading, setSignInLoading] = useState(false);
	const [signOutLoading, setSignOutLoading] = useState(false);
	const [signInErrored, setSignInErrored] = useState(false);
	const [signOutErrored, setSignOutErrored] = useState(false);

	const navigate = useNavigate();

	const signIn = useCallback((userName: string, password: string) => {
		setSignInLoading(true);
		axios.post<IUser>(`${process.env.REACT_APP_BASE_API_URL}api/users/authenticate`, { userName, password }, { withCredentials: true })
		.then(response => {
			setUser(response.data);
			navigate('/');
		})
		.catch(error => {
			setSignInErrored(true);
			console.error(error);
		})
		.finally(() => {
			setSignInLoading(false);
		})
	}, [navigate]);

	const signOut = useCallback(() => {
		setSignOutLoading(true);
		axios.delete(`${process.env.REACT_APP_BASE_API_URL}api/users/authenticate`, { withCredentials: true })
		.then(() => {
			setUser(undefined);
		})
		.catch(error => {
			setSignOutErrored(true);
			console.error(error);
		})
		.finally(() => {
			setSignOutLoading(false);
			navigate('/');
		})
	}, [navigate]);

	// Initially check for the user
	useEffect(() => {
		axios.get<IUser>(`${process.env.REACT_APP_BASE_API_URL}api/users/`, { withCredentials: true })
		.then(response => {
			setUser(response.data);

			if (!response.data && window.location.pathname.startsWith("/journals"))
				navigate('/signin/');
		})
		.catch(() => {
			console.log("User not signed in");
		})
	}, [navigate]);

	return (
		<AuthContext.Provider value={{ user, signIn, signOut, signInLoading, signOutLoading, signInErrored, signOutErrored }}>
			{children}
		</AuthContext.Provider>
	)

}