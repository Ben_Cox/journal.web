import { faSpinner, faArrowRight, faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Fireplace from "../../components/fireplace/fireplace";
import { AuthContext } from "../../contexts/authContext";
import { JournalSummary } from "../../types/journal";
import useStyles from "./styles";

const MAX_TITLE_LENGTH = 50;

const JournalsScreen: React.FC = () => {
	const classes = useStyles();

	const { user } = useContext(AuthContext);
	const [journals, setJournals] = useState<JournalSummary[]>([]);
	const [journalsLoading, setJournalsLoading] = useState(true);
	const [journalsErrored, setJournalsErrored] = useState(false);

	const [pageNumber, setPageNumber] = useState(1);

	// Load the journals initially
	useEffect(() => {
		axios.get<JournalSummary[]>(`${process.env.REACT_APP_BASE_API_URL}api/journals/summaries/?pageNumber=${encodeURIComponent(pageNumber)}&pageSize=15`, { withCredentials: true })
			.then(response => {
				setJournalsErrored(false);
				setJournals(response.data);
			})
			.catch(error => {
				console.log(error);
				setJournalsErrored(true);
			})
			.finally(() => {
				setJournalsLoading(false);
			})
	}, [pageNumber]);

	return (
		<Fireplace>
			<div className={classes.journals}>
				<Link className={classes.createButton} to='/journals/new/'>Create</Link>
				<div className={classes.paginationContainer}>
					<FontAwesomeIcon icon={faMinus} onClick={() => setPageNumber(prev => Math.max(1, prev - 1))} />
					<span>{pageNumber}</span>
					<FontAwesomeIcon icon={faPlus} onClick={() => setPageNumber(prev => prev + 1)} />
				</div>
				{journalsLoading && <FontAwesomeIcon className={classes.loadingIcon} size="lg" icon={faSpinner} fixedWidth spin />}
				{journalsErrored && <div>Something went wrong</div>}
				{!journalsLoading && !journalsErrored &&
					<>
						{journals.length === 0 && <div>You don't have any journals {pageNumber > 1 && "for this page"}</div>}
						{journals.length > 0 &&
							<div className={classes.journalsContainer}>
								{journals.map((j, i) =>
									<Link id={`journal-link-${j.id}`} to={`/journals/${j.id}/`} key={"journal" + i} className={classes.journalRow}>
										<span className={classes.journalName}>
											<span>{user?.userName === j.authorUserName ? "You" : j.authorUserName}</span>
											<span className={classes.journalDate}>{new Date(j.date).toLocaleDateString()}</span>
										</span>										
										<span className={classes.journalTitle}>
											{j.title.slice(0, Math.min(j.title.length, MAX_TITLE_LENGTH))}
											{j.title.length > MAX_TITLE_LENGTH && "..."}
										</span>
										<span className={classes.journalLink}><FontAwesomeIcon icon={faArrowRight} fixedWidth /></span>
									</Link>
								)}
							</div>
						}
					</>
				}
			</div>
		</Fireplace>
	);
};

export default JournalsScreen;