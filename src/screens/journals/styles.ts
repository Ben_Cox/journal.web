import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
	journals: {
		textAlign: "center",
		maxWidth: 1000,
		margin: "0 auto",
		position: "relative",
		padding: "0 10px",
	},
	loadingIcon: {
		display: "block",
		margin: "0 auto",
		marginBottom: 15,
	},
	createButton: {
		display: "inline-block",
		width: 30,
		margin: "0 auto",
		marginBottom: 15,
		padding: 15,
		borderRadius: 30,
		textDecoration: "none",
		minWidth: 75,
		textAlign: "center",
		color: "white",
		backgroundColor: "#ED0B6F",
		border: "1px solid #ED0B6F",
		transition: "background-color .4s ease",
		WebkitTransition: "background-color .4s ease",
		MozTransition: "background-color .4s ease",
		"&:hover": {
			color: "#ED0B6F",
			backgroundColor: "transparent",
		},
	},
	paginationContainer: {
		display: "inline-block",
		position: "absolute",
		right: 0,
		"& > span": {
			padding: "0 10px"
		},
	},
	journalsContainer: {

	},
	journalRow: {
		display: "flex",
		flexDirection: "row",
		flexWrap: "nowrap",
		alignItems: "center",
		backgroundColor: "white",
		maxWidth: 1000,
		margin: "0 auto",
		padding: 10,
		borderRadius: 25,
		marginBottom: 15,
		color: "black",
		textDecoration: "none",
		"& a": {
			color: "black",
			textDecoration: "none",
		},
		"&:hover": {
			"& $journalLink": {
				color: "white",
				backgroundColor: "#ED0B6F",

				// borderColor: "black",
				// color: "black",
				"& a": {
					color: "white",
				}
			}
		}
	},
	journalName: {
		margin: "5px 5px",
		padding: "5px 0",
		"& > span:first-child": {
			width: 90,
			backgroundColor: "#B0BB36",
			color: "white",
			padding: 10,
			borderRadius: 10,
			marginRight: 10,
		}
	},
	journalDate: {},
	journalTitle: {
		flexGrow: 1,
		marginLeft: 15,
		overflow: "hidden",
	},
	journalLink: {
		border: "2px solid transparent",
		textAlign: "right",
		color: "grey",
		textDecoration: "none",
		borderRadius: 10,
		transition: "background-color .4s ease",
		WebkitTransition: "background-color .4s ease",
		MozTransition: "background-color .4s ease",
	},
	"@media only screen and (max-width: 800px)": {
		paginationContainer: {
			right: 10,
		}
	}
});

export default useStyles;