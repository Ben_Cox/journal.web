import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
	existingJournal: {
		backgroundColor: "white",
		maxWidth: 1000,
		margin: "0 auto",
		padding: 10,
		borderRadius: 25,
		textAlign: "center",		
		position: "relative",
	},
});

export default useStyles;