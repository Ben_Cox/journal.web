import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Fireplace from "../../../components/fireplace/fireplace";
import JournalForm from "../../../components/journal/journalForm";
import { AuthContext } from "../../../contexts/authContext";
import { IJournal } from "../../../types/journal";
import useStyles from "./styles";

const ExistingJournalScreen: React.FC = () => {
	const classes = useStyles();

	const { id } = useParams();
	const navigate = useNavigate();

	const { user } = useContext(AuthContext);

	const [journal, setJournal] = useState<IJournal>();
	const [journalLoading, setJournalLoading] = useState(true);
	const [journalErrored, setJournalErrored] = useState(false);

	// Initially get the journal details for current page id
	useEffect(() => {
		if (id) {
			const idNumber = parseInt(id);
			if (isNaN(idNumber)) {
				alert("Something is wrong with the url");
				navigate('/journals');
			}
			else {				
				axios.get<IJournal>(`${process.env.REACT_APP_BASE_API_URL}api/journals/${encodeURIComponent(idNumber)}`, { withCredentials: true })
					.then(response => {
						setJournal(response.data);
					})
					.catch(error => {
						console.log(error);
						setJournalErrored(true);
					})
					.finally(() => {
						setJournalLoading(false);
					});
			}
		}
	}, [id, navigate])

	return (
		<Fireplace>
			<div className={classes.existingJournal}>
				<h1>{user?.userName === journal?.authorUserName ? "Your" : journal?.authorUserName} Journal</h1>
				<small>Editing not currently possible</small>
				{journalLoading && <FontAwesomeIcon icon={faSpinner} spin />}
				{!journalLoading && !journalErrored && <JournalForm readOnly journal={journal} />}
				{!journalLoading && journalErrored && <div>Sorry something has gone wrong</div>}
			</div>
		</Fireplace>
	);
};

export default ExistingJournalScreen;