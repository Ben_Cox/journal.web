import Fireplace from "../../../components/fireplace/fireplace";
import useStyles from "./styles";
import JournalForm from "../../../components/journal/journalForm";

const NewJournalScreen: React.FC = () => {
	const classes = useStyles();

	return (
		<Fireplace>
			<div className={classes.newJournal}>
				<h1>New Journal</h1>
				<JournalForm />
			</div>
		</Fireplace>
	);
}

export default NewJournalScreen;