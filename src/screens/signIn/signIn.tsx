import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useState } from "react";
import Fireplace from "../../components/fireplace/fireplace";
import { AuthContext } from "../../contexts/authContext";
import useStyles from "./styles";

interface ISignInForm {
	userName?: string,
	password?: string,
}

const SignInScreen: React.FC = () => {
	const classes = useStyles();

	const { signIn, signInLoading, signInErrored } = useContext(AuthContext);
	const [formData, setFormData] = useState<ISignInForm>({ userName: '', password: ''});
	const [formErrorMessages, setFormErrorMessages] = useState<ISignInForm>();


	const validateUserName = () => {
		let message: string | undefined = undefined;
		if (!formData?.userName)
			message = "Please provide a name";

		setFormErrorMessages(prev => ({ ...prev, userName: message }));

		return !!message;
	};

	const validatePassword = () => {
		let message: string | undefined = undefined;
		if (!formData?.password)
			message = "Please provide a password";

		setFormErrorMessages(prev => ({ ...prev, password: message }));

		return !!message;
	};

	const handleSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
		e.preventDefault(); // Don't refresh the page

		// Validation
		if (!validateUserName() && !validatePassword() && formData?.userName && formData.password)
			signIn(formData.userName, formData?.password);
	};

	return (
		<Fireplace>
			<div className={classes.signIn}>
				<h1>Sign In</h1>
				<form id="sign-in-form" onSubmit={handleSubmit} className={classes.form}>

					<label htmlFor="username-input">Name: </label>
					<input
						id="username-input"
						type="text"
						placeholder="Admin | Normal1 | Normal 2"
						value={formData?.userName}
						onChange={(e) => setFormData(prev => ({ ...prev, userName: e.target.value }))}
						onBlur={() => validateUserName()}
					/>
					{formErrorMessages?.userName && <div id="username-error-message" className={classes.errorMessage}>{formErrorMessages.userName}</div>}

					<label htmlFor="username-input">Password: </label>
					<input
						id="password-input"
						type="password"
						placeholder="Admin | Normal1 | Normal 2"
						value={formData?.password}
						onChange={(e) => setFormData(prev => ({ ...prev, password: e.target.value }))}
						onBlur={() => validatePassword()}
					/>
					{formErrorMessages?.password && <div id="password-error-message" className={classes.errorMessage}>{formErrorMessages.password}</div>}

					<button id="sign-in-submit" type="submit">{!signInLoading ? "Sign In" : <FontAwesomeIcon icon={faSpinner} fixedWidth spin /> }</button>
					{signInErrored && <div id="sign-in-error-message" className={classes.errorMessage}>Please check your username and password</div>}
				</form>
			</div>
		</Fireplace>
	);
};

export default SignInScreen;