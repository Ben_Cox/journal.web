import { useContext } from "react";
import { Link } from "react-router-dom";
import Fireplace from "../../components/fireplace/fireplace";
import { AuthContext } from "../../contexts/authContext";
import useStyles from "./styles";

const HomeScreen: React.FC = () => {
	const classes = useStyles();

	const { user } = useContext(AuthContext);

	return (
		<Fireplace>
			<div className={classes.hero}>
				<h1>Journal App</h1>
				{user &&
					<ul className={classes.badges}>
						<li className={classes.badgeYellow}>Welcome, {user.userName}</li>
					</ul>
				}
					<ul className={classes.badges}>
						<li className={classes.badgeGreen}>Full stack</li>
						<li className={classes.badgeGreen}>React</li>
						<li className={classes.badgeGreen}>.NET Web API</li>
						<li className={classes.badgeGreen}>SQL</li>
						<li className={classes.badgeGreen}>GitLab</li>
						<li className={classes.badgeGreen}>Azure</li>
					</ul>
				<div className={classes.instructionsContainer}>
					<h4>A typical workflow would be:</h4>
					<ul className={classes.instructions}>
						<li><Link id="hero-sign-in-link" to="/signin/" className={classes.link}>Sign in</Link> as <i>Normal1</i> or <i>Normal2</i></li>
						<li>View your <Link id="hero-journals-link" to="journals/" className={classes.link}>Journals</Link></li>
						<li>Add some <Link id="hero-new-journals-link" to="journals/new/" className={classes.link}>new Journals</Link></li>
						<li>Sign out</li>
						<li>Sign in as <i>Admin</i></li>
						<li>View <b>all</b> <Link id="hero-journals-link" to="journals/" className={classes.link}>Journals</Link></li>
						<li>Hire <i>Ben Cox™</i> 😉</li>
					</ul>
					<h4>Project links:</h4>
					<ul className={classes.instructions}>
						<li><a className={classes.link} href={"https://gitlab.com/Ben_Cox/journal.web/"} target="_blank" rel="noreferrer">GitLab</a> front-end codebase</li>
						<li><a className={classes.link} href={"https://gitlab.com/Ben_Cox/journal.api/"} target="_blank" rel="noreferrer">GitLab</a> back-end codebase</li>
						<li><a className={classes.link} href={"/public/Journal App.docx"} target="_blank" rel="noreferrer">Project Brief</a></li>
					</ul>
				</div>
			</div>
		</Fireplace>
	);
};

export default HomeScreen;