import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
	hero: {
		backgroundColor: "white",
		maxWidth: 1000,
		margin: "0 auto",
		padding: 10,
		textAlign: "center",		
		borderRadius: 25,

		"& h4": {
			margin: 0,
			marginBottom: 5
		}
	},
	badges: {
		listStyle: "none",
		padding: 0,
	},
	badgeGreen: {
		backgroundColor: "#B0BB36",
		display: "inline-block",
		color: "white",
		padding: 10,
		borderRadius: 10,
		margin: "5px 5px",
	},
	badgeYellow: {
		display: "inline-block",
		backgroundColor: "#FCAE17",
		color: "white",
		padding: 10,
		borderRadius: 10,
		margin: "5px 5px",	
	},
	instructionsContainer: {
		textAlign: "left",
		margin: "0 auto",
		width: 250,
		"& h4:not(:first-child)": {
			marginTop: 20,
		}
	},
	instructions: {
		margin: "0 auto",
		"& li": {
			marginBottom: 5,
		},
	},
	link: {
		color: "black; !important",
		textDecoration: "underline",
		"&:hover": {
			textDecoration: "none",
		}
	},
	countdownContainer: {
		backgroundColor: "#B0BB36",
		padding: 20,
		margin: 10,
		borderRadius: 10,
		textAlign: "center",

	}
});

export default useStyles;